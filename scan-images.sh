#!/usr/bin/env sh
# =========================================================================================
# Copyright (C) 2021 Orange & contributors
#
# This program is free software; you can redistribute it and/or modify it under the terms
# of the GNU Lesser General Public License as published by the Free Software Foundation;
# either version 3 of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License along with this
# program; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
# Floor, Boston, MA  02110-1301, USA.
# =========================================================================================

set -e

function log_info() {
  echo -e "[\\e[1;94mINFO\\e[0m] $*"
}

function log_warn() {
  echo -e "[\\e[1;93mWARN\\e[0m] $*"
}

function log_error() {
  echo -e "[\\e[1;91mERROR\\e[0m] $*"
}

function fail() {
  log_error "$@"
  exit 1
}

mkdir -p -m 777 reports
mkdir -p public/secu

while read -r line
do
  tmpl_name=$(echo "$line" | cut -d '|' -f1)
  img_usage=$(echo "$line" | cut -d '|' -f2)
  var_name=$(echo "$line" | cut -d '|' -f3)
  img_uri=$(echo "$line" | cut -d '|' -f4)
  log_info "--- scanning ($img_usage) \\e[33;1m${var_name}\\e[0m image for \\e[33;1m${tmpl_name}\\e[0m template: \\e[32m${img_uri}\\e[0m"
  # MkDocs format
  trivy image --cache-dir .cache --scanners vuln --format template --exit-code 0 --template "@trivy-report.tpl" --output "docs/secu/trivy-${var_name}.md" "$img_uri" || log_warn "Failed"
  # # GitLab format
  # trivy image --scanners vuln --format template --exit-code 0 --template "@/contrib/gitlab.tpl" --output "reports/trivy-${var_name}.gitlab.json" "$img_uri" || log_warn "failed"
  # # text format (stdout)
  # trivy image --scanners vuln --format table --exit-code 0 "$img_uri" || log_warn "failed"
done < ./tbc-default-images.out