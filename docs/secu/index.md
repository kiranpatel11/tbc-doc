# Security

!!! WARNING

    _to be continuous_ templates use to embed required tools as **container images**.

    As much as we can, we try to select either **official images** (ex: Maven, Python), or at least images maintained by an **active community**.

    Each of those images can be freely overridden with the appropriate configuration variable to select _fixed versions_ ([more info here](../usage.md#docker-images-versions)) or any alternative that would suit you more. 

    _to be continuous_ is not responsible of any possible security issue from a default container image.
    If a vulnerability would be found in an image used by default, you could address it in one of the following ways:

    - report the vulnerability to the owning project,
    - select an other version or alternative of the image that fixes the issue,
    - build and use your own, vulnerability-free image.

## Vulnerability Reports (Trivy)

Here are vulnerability reports for each default image used by _to be continuous_ templates (generated every day):

<table>
<thead><tr>
<th>Template</th>
<th>Image Variable</th>
<th>Default Image</th>
</tr></thead>
<tbody>
--8<-- "docs/secu/trivy-reports-body.part.html"
</tbody>
</table>